<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login',  'apilogin@index');
//customer api
Route::post('/customerlist', 'apicustomer@list');
Route::post('/addcustomer', 'apicustomer@add');
Route::post('/customerdetails', 'apicustomer@getcustomerdetails');
Route::post('/editcustomer', 'apicustomer@edit');
Route::post('/deletecustomer', 'apicustomer@delete');

//taxtype api
Route::post('/taxtypelist', 'apitaxtype@list');
Route::post('/addtaxtype', 'apitaxtype@add');
Route::post('/taxtypedetails', 'apitaxtype@getdetails');
Route::post('/edittaxtype', 'apitaxtype@edit');
Route::post('/deletetaxtype', 'apitaxtype@delete');
//product unit api
Route::post('/productunitlist', 'apiproductunit@list');
Route::post('/addproductunit', 'apiproductunit@add');
Route::post('/productunitdetails', 'apiproductunit@getdetails');
Route::post('/editproductunit', 'apiproductunit@edit');
Route::post('/deleteproductunit', 'apiproductunit@delete');

//currency unit api
Route::post('/currencylist', 'apicurrencies@list');

//product  api
Route::post('/productlist', 'apiproduct@list');
Route::post('/addproduct', 'apiproduct@add');
Route::post('/productdetails', 'apiproduct@getdetails');
Route::post('/editproduct', 'apiproduct@edit');
Route::post('/deleteproduct', 'apiproduct@delete');


