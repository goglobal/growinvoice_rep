
<script src="{{ asset('nobelUI/assets/vendors/core/core.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/chartjs/Chart.min.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/jquery.flot/jquery.flot.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/jquery.flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/progressbar.js/progressbar.min.js') }}"></script>
<script src="{{ asset('nobelUI/assets/vendors/feather-icons/feather.min.js') }}"></script>
<script src="{{ asset('nobelUI/assets/js/template.js') }}"></script>
<script src="{{ asset('nobelUI/assets/js/dashboard.js') }}"></script>
<script src="{{ asset('nobelUI/assets/js/datepicker.js') }}"></script>

@yield('page_body_scripts')



