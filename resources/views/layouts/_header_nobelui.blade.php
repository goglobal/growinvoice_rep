
  <!-- partial:partials/_navbar.html -->
      <nav class="navbar">
        <a href="#" class="sidebar-toggler">
          <i data-feather="menu"></i>
        </a>
        <div class="navbar-content">
        
             

          <ul class="navbar-nav">
             @if (count($languages) > 1)
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flag-icon flag-icon-us mt-1" title="us"></i> <span class="font-weight-medium ml-1 mr-1 d-none d-md-inline-block"> {{ app()->getLocale() }}</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="languageDropdown">

                
                  @foreach ($languages as $language => $name)
                 <a href="/change-language/{{ $language }}" class="dropdown-item py-2"><i class="flag-icon flag-icon-us" title="us" id="us"></i> <span class="ml-1"> {{ $name }} </span></a>
                  @endforeach

                    </div>
                  </li>
           @endif
              <li class="nav-item dropdown nav-profile">
              <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="https://via.placeholder.com/30x30" alt="profile">
              </a>
              <div class="dropdown-menu" aria-labelledby="profileDropdown">
                <div class="dropdown-header d-flex flex-column align-items-center">
                  <div class="figure mb-3">
                    <img src="{{ $authUser->avatar }}" alt="">
                  </div>
                  <div class="info text-center">
                    <p class="name font-weight-bold mb-0">{{ $authUser->full_name }}</p>
                    </div>
                </div>
                <div class="dropdown-body">
                  <ul class="profile-nav p-0 pt-3">
                   
                     @if(!$authUser->hasRole('super_admin'))
                    <li class="nav-item">
                      <a href="{{ route('settings.account', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">
                        <i data-feather="edit"></i>
                        <span>{{ __('messages.my_profile') }}</span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('settings.company', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">
                        <i data-feather="repeat"></i>
                        <span>{{ __('messages.company') }}</span>
                      </a>
                    </li>
                    @else
                      <li class="nav-item">
                      <a href="{{ route('super_admin.users.edit', $authUser->id) }}" class="nav-link">
                        <i data-feather="repeat"></i>
                        <span>{{ __('messages.my_profile') }}</span>
                      </a>
                    </li>

                     @endif
                    <li class="nav-item">
                      <a href="{{ route('logout') }}" class="nav-link">
                        <i data-feather="log-out"></i>
                        <span>{{ __('messages.logout') }}</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>                              
            </ul>

           </div>
         </nav>

           <!-- partial -->
