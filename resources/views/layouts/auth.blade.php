<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    @include('layouts._css_nobelui')
</head>
<!--
<body class="layout-login-centered-boxed">
    <div class="layout-login-centered-boxed__form card">
        <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-2 navbar-light">
            <a href="{{ url('/login') }}" class="navbar-brand flex-column mb-2 align-items-center mr-0">
                @if(get_system_setting('application_logo'))
                    <img class="navbar-brand-icon" src="{{ get_system_setting('application_logo') }}" width="125" alt="{{ get_system_setting('application_name') }}">
                @else
                    <span>{{ get_system_setting('application_name') }}</span>
                @endif
            </a>
        </div>
-->

<div class="main-wrapper">
        <div class="page-wrapper full-page">
            <div class="page-content d-flex align-items-center justify-content-center">

                <div class="row w-100 mx-0 auth-page">
                    <div class="col-md-8 col-xl-6 mx-auto">
                        <div class="card">
                            <div class="row">
                <div class="col-md-4 pr-md-0">
                  <div class="auth-left-wrapper">

                  </div>
                </div>





        @yield('content')
</div>
    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        
    <!-- </div>-->

    @include('layouts._js_nobelui')
    @include('layouts._flash')
</body>
</html>
