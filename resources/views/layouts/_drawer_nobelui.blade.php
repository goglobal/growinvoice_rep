


<!-- partial:partials/_sidebar.html -->
   <nav class="sidebar">
      <div class="sidebar-header">
        <a href="#" class="sidebar-brand">
          Noble<span>UI</span>
        </a>
        <div class="sidebar-toggler not-active">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <div class="sidebar-body">
        <ul class="nav">
             @if($authUser->hasRole('super_admin'))

        <li class="nav-item nav-category">Super Admin Menu</li>

          <li class="nav-item">
            <a href="{{ route('super_admin.dashboard') }}" class="nav-link">
              <i class="link-icon" data-feather="box"></i>
              <span class="link-title">{{ __('messages.dashboard') }}</span>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('super_admin.users') }}" class="nav-link">
              <i class="link-icon" data-feather="users"></i>
              <span class="link-title">{{ __('messages.users') }}</span>
            </a>
          </li>

           <li class="nav-item">
            <a href="{{ route('super_admin.plans') }}" class="nav-link">
              <i class="link-icon" data-feather="layout"></i>
              <span class="link-title">{{ __('messages.plans') }}</span>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('super_admin.pages') }}" class="nav-link">
              <i class="link-icon" data-feather="edit"></i>
              <span class="link-title">{{ __('messages.pages') }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('super_admin.subscriptions') }}" class="nav-link">
              <i class="link-icon" data-feather="grid"></i>
              <span class="link-title">{{ __('messages.subscriptions') }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('super_admin.orders') }}" class="nav-link">
              <i class="link-icon" data-feather="shopping-cart"></i>
              <span class="link-title">{{ __('messages.orders') }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('super_admin.languages') }}" class="nav-link">
              <i class="link-icon" data-feather="globe"></i>
              <span class="link-title">{{ __('messages.languages') }}</span>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('super_admin.settings') }}" class="nav-link">
              <i class="link-icon" data-feather="settings"></i>
              <span class="link-title">{{ __('messages.settings') }}</span>
            </a>
          </li>


          <li class="nav-item nav-category">{{ __('messages.settings') }}</li>
           
           <li class="nav-item">
            <a href="{{ route('super_admin.settings.application') }}" class="nav-link">
              <i class="link-icon" data-feather="aperture"></i>
              <span class="link-title">{{ __('messages.application_settings') }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('super_admin.settings.mail') }}" class="nav-link">
              <i class="link-icon" data-feather="mail"></i>
              <span class="link-title">{{ __('messages.mail_settings') }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('super_admin.settings.payment') }}" class="nav-link">
              <i class="link-icon" data-feather="file-text"></i>
              <span class="link-title">{{ __('messages.payment_settings') }}</span>
            </a>
          </li>
        <li class="nav-item">
            <a href="{{ route('super_admin.settings.theme', get_system_setting('theme')) }}" class="nav-link">
              <i class="link-icon" data-feather="settings"></i>
              <span class="link-title">{{ __('messages.theme_settings') }}</span>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('super_admin.settings.cron') }}" class="nav-link">
              <i class="link-icon" data-feather="server"></i>
              <span class="link-title">{{ __('messages.cron_configuration') }}</span>
            </a>
          </li>

        <li class="nav-item nav-category">{{ __('messages.logout') }}</li>
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link">
              <i class="link-icon" data-feather="log-out"></i>
              <span class="link-title">{{ __('messages.logout') }}</span>
            </a>
          </li> 
           
         @else
     
          <li class="nav-item nav-category"> <strong>{{ $currentCompany->name }}</strong> </li>

           <li class="nav-item nav-category"> Menu </li>
         
            <li class="nav-item">
            <a href="{{ route('dashboard', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">
              <i class="link-icon" data-feather="box"></i>
              <span class="link-title">{{ __('messages.dashboard') }}</span>
            </a>
          </li>  
           <li class="nav-item">
            <a href="{{ route('customers', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">
              <i class="link-icon" data-feather="users"></i>
              <span class="link-title">{{ __('messages.customers') }}</span>
            </a>
             </li>  
          <li class="nav-item">
            <a href="{{ route('products', ['company_uid' => $currentCompany->uid])  }}" class="nav-link">
              <i class="link-icon" data-feather="package"></i>
              <span class="link-title">{{ __('messages.products') }}</span>
            </a>
          </li>  
          <li class="nav-item">
            <a href="{{ route('invoices', ['company_uid' => $currentCompany->uid])  }}" class="nav-link">
              <i class="link-icon" data-feather="file-plus"></i>
              <span class="link-title">{{ __('messages.invoices') }}</span>
            </a>
          </li>  
        <li class="nav-item">
            <a href="{{ route('estimates', ['company_uid' => $currentCompany->uid])  }}" class="nav-link">
              <i class="link-icon" data-feather="file"></i>
              <span class="link-title">{{ __('messages.estimates') }}</span>
            </a>
          </li>  
 
           <li class="nav-item">
            <a href="{{ route('payments', ['company_uid' => $currentCompany->uid])  }}" class="nav-link">
              <i class="link-icon" data-feather="file-text"></i>
              <span class="link-title">{{ __('messages.payments') }}</span>
            </a>
          </li>  
           <li class="nav-item">
            <a href="{{ route('expenses', ['company_uid' => $currentCompany->uid])  }}" class="nav-link">
              <i class="link-icon" data-feather="file-minus"></i>
              <span class="link-title">{{ __('messages.expenses') }}</span>
            </a>
          </li>  
            <li class="nav-item">
            <a href="{{ route('vendors', ['company_uid' => $currentCompany->uid])  }}" class="nav-link">
              <i class="link-icon" data-feather="user-check"></i>
              <span class="link-title">{{ __('messages.vendors') }}</span>
            </a>
          </li>  
 
        @if($currentCompany->subscription('main')->getFeatureValue('view_reports') === '1')
                  <li class="nav-item nav-category"> reports </li>
                   <li class="nav-item">

                     <a class="nav-link" data-toggle="collapse" href="#reports" role="button" aria-expanded="false" aria-controls="reports">
              <i class="link-icon" data-feather="book"></i>
              <span class="link-title">{{ __('messages.reports') }}</span>
              <i class="link-arrow" data-feather="chevron-down"></i>
            </a>
            
          
            <div class="collapse" id="reports">
              <ul class="nav sub-menu">
                <li class="nav-item">
                  <a href="{{ route('reports.customer_sales', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">{{ __('messages.customer_sales') }}</a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('reports.product_sales', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">{{ __('messages.product_sales') }}</a>
                   </li>
                <li class="nav-item">
                  <a href="{{ route('reports.profit_loss', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">{{ __('messages.profit_loss') }}</a>
                  </li>
                <li class="nav-item">
                  <a href="{{ route('reports.expenses', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">{{ __('messages.expenses') }}</a>
                  </li>
                  <li class="nav-item">
                  <a href="{{ route('reports.vendors', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">{{ __('messages.vendors') }}</a>
                  </li>
                 @endif
              </ul>
            </div>
          </li>   
       



  <li class="nav-item nav-category">{{ __('messages.settings') }}</li>
   
        <li class="nav-item">
            <a href="{{ route('settings.account', ['company_uid' => $currentCompany->uid]) }}" class="nav-link">
              <i class="link-icon" data-feather="settings"></i>
              <span class="link-title">{{ __('messages.settings') }}</span>
            </a>
       </li>  

     <li class="nav-item nav-category">{{ __('messages.logout') }}</li>
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link">
              <i class="link-icon" data-feather="log-out"></i>
              <span class="link-title">{{ __('messages.logout') }}</span>
            </a>
          </li>  
                        
    @endif
   </ul>
      </div>
    </nav>
