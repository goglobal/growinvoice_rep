<link type="text/css" href="{{ asset('nobelUI/assets/vendors/core/core.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('nobelUI/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('nobelUI/assets/fonts/feather-font/css/iconfont.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('nobelUI/assets/vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('nobelUI/assets/fonts/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('assets/css/vendor-material-icons.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('assets/css/vendor-fontawesome-free.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('nobelUI/assets/css/demo_1/style.css') }}" rel="stylesheet">







@if(get_system_setting('application_favicon'))
<!-- Favicon -->
<link rel="icon" href="{{ get_system_setting('application_favicon') }}">
@endif

<!-- company based preferences -->
@shared
<!-- END company based preferences -->

<!-- page based scripts & styles -->
@yield('page_head_scripts')
<!-- END page based scripts & styles -->
