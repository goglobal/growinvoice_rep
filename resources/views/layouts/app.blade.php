<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

   <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    @include('layouts._css_nobelui1')
</head>
<body>
    <div class="main-wrapper">
     @include('layouts._drawer_nobelui')
   
<div class="page-wrapper">
@include('layouts._header_nobelui')

<div class="page-content">

    
 <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
          <div>
            @yield('page_header')
           </div>
  
     </div>

       

       @yield('content')   





</div>


    <!-- partial:partials/_footer.html -->
            <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
                <p class="text-muted text-center text-md-left">Copyright © 2021 <a href="#" target="_blank">NobleUI</a>. All rights reserved</p>
             <!--    <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p> -->
            </footer>
    <!-- partial -->

</div>
    </div>

    @include('layouts._js_nobelui')
    @include('layouts._flash')
</body>
     @yield('modal')
</html>
