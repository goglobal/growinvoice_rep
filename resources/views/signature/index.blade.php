@extends('layouts.app', ['page' => 'signature'])

@section('title', __('signature'))
    
@section('page_header')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">signature</li>
                </ol>
            </nav>
            <h1 class="m-0"></h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="card">
        <canvas></canvas>
    </div>
    <script>
        var canvas = document.querySelector("canvas");
        var signaturePad = new SignaturePad(canvas);
        signaturePad.toDataURL(); // save image as PNG
        signaturePad.toDataURL("image/jpeg"); // save image as JPEG
        signaturePad.toDataURL("image/svg+xml"); // save image as SVG
        signaturePad.clear();
        signaturePad.isEmpty();
        signaturePad.off();
        signaturePad.on();
    </script>
@endsection
