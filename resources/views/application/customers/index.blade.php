@extends('layouts.app', ['page' => 'customers'])

@section('title', __('messages.customers'))
    
@section('page_header')
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('messages.customers') }}</li>
                </ol>
            </nav>
            <h1 class="m-0">{{ __('messages.customers') }}</h1>
        </div>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
  <i class="material-icons">add</i> {{ __('messages.create_customer') }}
</button>
       <!--  <a href="{{ route('customers.create', ['company_uid' => $currentCompany->uid]) }}" class="btn btn-success ml-3"><i class="material-icons">add</i> {{ __('messages.create_customer') }}</a>
 -->    </div>
@endsection

@section('content')
    @include('application.customers._filters')

    <div class="card">
        @include('application.customers._table')
    </div>

  

@endsection

<!-- Button trigger modal -->

@section('modal') 

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __('messages.create_customer') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form action="{{ route('customers.store', ['company_uid' => $currentCompany->uid]) }}" method="POST" enctype="multipart/form-data">
          @csrf
     <div class="row">
                <div class="col">
                    <div class="form-group required">
                             <input name="display_name" type="text" class="form-control" placeholder="{{ __('messages.display_name') }}" value="{{ $customer->display_name }}" required>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group required">
                              <input name="contact_name" type="text" class="form-control" placeholder="{{ __('messages.contact_name') }}" value="{{ $customer->contact_name }}" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group required">
                              <input name="email" type="email" class="form-control" placeholder="{{ __('messages.email') }}" value="{{ $customer->email }}" required>
                    </div>
                </div>
                
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group required">
                            <select name="currency_id" data-toggle="select" class="form-control select2-accessible" data-select2-id="currency_id" required>
                            <option disabled selected>{{ __('messages.select_currency') }}</option>
                            @foreach(get_currencies_select2_array() as $option)
                                <option value="{{ $option['id'] }}" {{ $customer->currency_id == $option['id'] ? 'selected=""' : '' }}>{{ $option['text'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-group">
                             <input name="vat_number" type="text" class="form-control" placeholder="GST NO ( For India ) / {{ __('messages.vat_number') }}" value="{{ $customer->vat_number }}">
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col">
                    <div class="form-group">
              <input name="billing[name]" type="text" class="form-control billingname" placeholder="Billing {{ __('messages.address_name') }}"  required>
               </div>
                </div>
               </div> 

            <div class="row">
                <div class="col">
                    <div class="form-group required">
                          <select id="billing[country_id]" name="billing[country_id]" data-toggle="select" class="form-control select2-accessible billingcountry_id" data-select2-id="billing[country_id]" required>
                            <option disabled selected>{{ __('messages.select_country') }}</option>
                            @foreach(get_countries_select2_array() as $option)
                                <option value="{{ $option['id'] }}" {{ $customer->billing->country_id == $option['id'] ? 'selected=""' : '' }}>{{ $option['text'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
         </div>
          <div class="row">
             <div class="col">
                    <div class="form-group required">
                  <input name="billing[address_1]" class="form-control billingaddress_1"  placeholder="{{ __('messages.address') }}"  required>
                   </div>
                </div>
        </div>
       <button type="submit" class="btn btn-primary">{{ __('messages.save_customer') }}</button>
   

  @if($customer->getCustomFields()->count() > 0)
        <div class="row no-gutters">
            <div class="col-lg-4 card-body">
                <p><strong class="headings-color">{{ __('messages.custom_fields') }}</strong></p>
            </div>
            <div class="col-lg-8 card-form__body card-body">
                <div class="row">
                    @foreach ($customer->getCustomFields() as $custom_field)
                        @include('layouts._custom_field', ['model' => $customer, 'custom_field' => $custom_field])
                    @endforeach
                </div>
            </div>
        </div>
    @endif

      </form>


      </div>
      
    </div>
  </div>
</div>
@endsection