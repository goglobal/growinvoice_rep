<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;



class apiproduct extends Controller
{
 

     public function list(Request $request)
    {

    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
          $products = DB::table('products')
          ->join('companies', 'companies.id', '=', 'products.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')
          ->leftJoin('product_units','product_units.id','=','products.unit_id')
          ->leftJoin('currencies','currencies.id','=','products.currency_id')       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('products.id as productid','products.name','products.unit_id','product_units.name as unitname','products.description','products.price','products.currency_id','currencies.name as currencyname', 'products.ProductImage','products.HSNCode','products.StockWarning','products.Category','products.Stock','products.WholesalePrice','products.WholesaleQty','products.PurchasePrice','products.SellPrice','products.MRP','products.SKUCode',
        DB::raw('null as tax_type')
      )
         ->get();

       

foreach ($products as $addct)
     {
         $query= DB::table('taxes')
            ->leftJoin('tax_types' ,'tax_types.id','=', 'taxes.tax_type_id')
            ->where([
             [ 'taxes.taxable_id' ,'=',$addct->productid ],
            [ 'taxes.taxable_type','=','App\Models\Product'],   
          
          ])
          ->select('taxes.tax_type_id','tax_types.name')
          ->get();
          $addct->tax_type= $query->toArray();
         
     

     }

  
         
     return [ "products List"=> $products];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
   }

      return ["message" =>$message];
    } 

 public function getdetails(Request $request)
    {


    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token) && isset($data->productid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {

           $products = DB::table('products')
          ->join('companies', 'companies.id', '=', 'products.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')
          ->leftJoin('product_units','product_units.id','=','products.unit_id')
          ->leftJoin('currencies','currencies.id','=','products.currency_id')       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token],
           [ 'products.id' ,'=',$data->productid]
          ])
          ->select('products.id as productid','products.name','products.unit_id','product_units.name as unitname','products.description','products.price','products.currency_id','currencies.name as currencyname','products.ProductImage','products.HSNCode','products.StockWarning','products.Category','products.Stock','products.WholesalePrice','products.WholesaleQty','products.PurchasePrice','products.SellPrice','products.MRP','products.SKUCode',
        DB::raw('null as tax_type')
      )
         ->get();

        foreach ($products as $addct)
        {
         $query= DB::table('taxes')
            ->leftJoin('tax_types' ,'tax_types.id','=', 'taxes.tax_type_id')
            ->where([
             [ 'taxes.taxable_id' ,'=',$addct->productid ],
            [ 'taxes.taxable_type','=','App\Models\Product'],   
          
          ])
          ->select('taxes.tax_type_id','tax_types.name')
          ->get();
          $addct->tax_type= $query->toArray();
         
         }

  
         
     return [ "products List"=> $products];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
         } 
        return ["message" =>$message];
    }

/////////////////add

   public function add(Request $request)
     {
     $data =(object)$request->json()->all();
    
     $message=" "; 

if( isset($data->uid)  && isset($data->token)&& isset($data->name)&& isset($data->unit_id)&& isset($data->description)&& isset($data->price)&& isset($data->tax_type))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {


     
        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();

 $custcount = DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_subscription_usage.used')        
         ->get();

$ctAddcount=0;
$compid=0;
 foreach ($custAddcount as $addct)
    {
        $ctAddcount=(int)$addct->value;
        $compid=$addct->id;
    }


$ctcount=0;
 foreach ($custcount as $ct)
    {
        $ctcount=(int)$ct->used;
    }

         if(   $ctcount  < $ctAddcount )
         {

      $product = Product::create([
            'name' => $data->name,
            'company_id' => $compid,
            'unit_id' => $data->unit_id,
            'price'  => $data->price,
            'description' => $data->description,
            'ProductImage'=> $data->ProductImage,
            'HSNCode'=> $data->HSNCode,
            'SKUCode'=> $data->SKUCode,
            'StockWarning'=> $data->StockWarning,
            'Category'=> $data->Category,
            'Stock'=> $data->Stock,
            'WholesalePrice'=> $data->WholesalePrice,
            'WholesaleQty'=> $data->WholesaleQty,
            'PurchasePrice'=> $data->PurchasePrice,
            'SellPrice'=> $data->SellPrice,
            'MRP'=> $data->MRP,
        ]);

       
        // // Add Product Taxes
             foreach ($data->tax_type as $tax) {

                $product->taxes()->create([
                    'tax_type_id' => $tax['tax_type_id']
                ]);
            }
      

       DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_subscription_usage.used')        
         ->update(['plan_subscription_usage.used' => ($ctcount+ 1)]);

        return [ "Add product successful."];


         }

            return [ "Cannot add product. Subscription Plan Limit Exceed."];


    
         }

         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}


  public function edit(Request $request)
    {
     $data =(object)$request->json()->all();
    
     $message=" "; 

if( isset($data->uid)  && isset($data->token)&& isset($data->name)&& isset($data->unit_id)&& isset($data->description)&& isset($data->price)&& isset($data->tax_type)&&isset($data->productid)  )
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
  
        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();

    $ctAddcount=0;
    $compid=0;
     foreach ($custAddcount as $addct)
        {
            $ctAddcount=(int)$addct->value;
            $compid=$addct->id;
        }


        $ctcount=0;


      $product = Product::where([['company_id', '=', $compid] ,

                                    ['id','=',$data->productid]]
                                    )->firstOrFail();
        // Update the Expense
        $product->update([
            'name' => $request->name,
            'unit_id' => $request->unit_id,
            'price'  => $request->price,
            'description' => $request->description,
            'description' => $data->description,
            'ProductImage'=> $data->ProductImage,
            'HSNCode'=> $data->HSNCode,
            'SKUCode'=> $data->SKUCode,
            
            'StockWarning'=> $data->StockWarning,
            'Category'=> $data->Category,
            'Stock'=> $data->Stock,
            'WholesalePrice'=> $data->WholesalePrice,
            'WholesaleQty'=> $data->WholesaleQty,
            'PurchasePrice'=> $data->PurchasePrice,
            'SellPrice'=> $data->SellPrice,
            'MRP'=> $data->MRP,
        ]);

       

      // Remove old Product Taxes
        $product->taxes()->delete();
       
        // // Add Product Taxes
             foreach ($data->tax_type as $tax) {

                $product->taxes()->create([
                    'tax_type_id' => $tax['tax_type_id']
                ]);
            }
      
 
        return [ "Update product successful."];
         }

         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
      } 
     return ["message" =>$message];
    }




public function delete(Request $request)
    {
    $data =(object)$request->json()->all();
    
     $message=" "; 

if( isset($data->uid)  && isset($data->token)&& isset($data->productid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {


        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();


    $custcount = DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_subscription_usage.used')        
         ->get();

$ctAddcount=0;
$compid=0;
 foreach ($custAddcount as $addct)
    {
        $ctAddcount=(int)$addct->value;
        $compid=$addct->id;
    }


$ctcount=0;
 foreach ($custcount as $ct)
    {
        $ctcount=(int)$ct->used;
    }

$ct_tt=DB::table('products')->where([
                                    ['company_id','=',$compid],
                                    ['id','=',$data->productid]
                                    ])->select('tax_types.*')        
                                     ->count() ;
           if($ct_tt>0  ) 
           {    


      $product = Product::where([['company_id', '=', $compid] ,
                                ['id','=',$data->productid]]
                                    )->firstOrFail();

         if ($product->invoice_items()->exists() && $product->invoice_items()->count() > 0) {
                return [ 'product cant deleted invoice exists' ];
        }

        // If the product already in use in Estimate Items
        // then return back and flash an alert message
        if ($product->estimate_items()->exists() && $product->estimate_items()->count() > 0) {
            return [' product cant deleted estimate exists.'];                  

                  }

        // Delete Product Taxes from Database
        if ($product->taxes()->exists() && $product->taxes()->count() > 0) {
            $product->taxes()->delete();
        }

        // Delete Product from Database
        $product->delete();

       
      

       DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','products']
          ])
          ->select('plan_subscription_usage.used')        
         ->update(['plan_subscription_usage.used' => ($ctcount- 1)]);

        return [ "Delete product successful."];
}else{

return [ "product id doesnot exist."];

}
         }

         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}




}
