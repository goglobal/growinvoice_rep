<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\DB;



class apicurrencies extends Controller
{
    //
      public function list(Request $request)
    {


    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
     $currencies = DB::table('currencies')
     ->select('currencies.id as currencyid','currencies.name','currencies.code','currencies.symbol','currencies.precision','currencies.thousand_separator','currencies.decimal_separator','currencies.swap_currency_symbol')        
                ->get();
  
         
     return [ "currency List"=> $currencies];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 

}
