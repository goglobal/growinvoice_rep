<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TaxType;
use App\Http\Requests\Application\Settings\TaxType\Store;
use App\Http\Requests\Application\Settings\TaxType\Update;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;




class apitaxtype extends Controller
{
    //



     public function list(Request $request)
    {

    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
     $tax_types = DB::table('tax_types')
            ->join('companies', 'companies.id', '=', 'tax_types.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')
        
       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('tax_types.id as tax_typeid','tax_types.name','tax_types.percent','tax_types.description')
        
         ->get();
  
         
     return [ "tax_type List"=> $tax_types];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 

 public function getdetails(Request $request)
    {


    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token) && isset($data->tax_typeid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {

         $tax_types = DB::table('tax_types')
            ->join('companies', 'companies.id', '=', 'tax_types.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')      
       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token],
           [ 'tax_types.id','=',$data->tax_typeid]
          ])
          ->select('tax_types.id as tax_typeid','tax_types.name','tax_types.percent','tax_types.description')
          ->get();

    
  
         
     return [ "tax_type details"=> $tax_types];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 




  public function add(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->name)&& isset($data->percent)&& isset($data->description))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {



         $currentCompanyid=0;


        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('companies.id')        
         ->get();


         foreach ($custAddcount as $addct)
         {
          $currentCompanyid=$addct->id;
         }

          // // Create Tax Type and Store in Database
        TaxType::create([
            'name' => $request->name,
            'company_id' => $currentCompanyid,
            'percent' => $request->percent,
            'description' => $request->description,
        ]);



        return ["message" => "Add taxtype successful."];


         }

         else{

            $message=" Authentication error.";

          }
         }else{

         $message=" Send Format is not correct.";
     

      } 
 return ["message" =>$message];
    }


  public function edit(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->name)&& isset($data->percent)&& isset($data->description)&& isset($data->tax_typeid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
        $currentCompanyid=0;


        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('companies.id')        
         ->get();


         foreach ($custAddcount as $addct)
         {
          $currentCompanyid=$addct->id;
         }

 $ct_tt=DB::table('tax_types')->where([
                                    ['company_id','=',$currentCompanyid],
                                    ['id','=',$data->tax_typeid]
                                    ])->select('tax_types.*')        
                                     ->count() ;
           if($ct_tt>0  ) 
           {    


         $TaxType =  TaxType::where([
                                    ['company_id','=',$currentCompanyid],
                                    ['id','=',$data->tax_typeid]
                                    ])->firstOrFail();

       
        $TaxType->update([
            'name' => $request->name,
            'percent' => $request->percent,
            'description' => $request->description,
        ]);

  

        return ["message" => "Update customer successful."];

        }else{

           return [ "message" =>"Taxtype Update unsuccessful.Record not found."];
  
        }
        

    
        }else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}

public function delete(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->tax_typeid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {

        $currentCompanyid=0;


        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('companies.id')        
         ->get();


         foreach ($custAddcount as $addct)
         {
          $currentCompanyid=$addct->id;
         }


        $ct_tt=DB::table('tax_types')->where([
                                    ['company_id','=',$currentCompanyid],
                                    ['id','=',$data->tax_typeid]
                                    ])->select('tax_types.*')        
                                     ->count() ;
           if($ct_tt>0  ) 
           {    


         $TaxType =  TaxType::where([
                                    ['company_id','=',$currentCompanyid],
                                    ['id','=',$data->tax_typeid]
                                    ])->firstOrFail();

       


        // Check if the Tax is already in use
        if ($TaxType->taxes() && $TaxType->taxes()->count() > 0) {
            return [ "message" =>"Taxtype is in use, delete not possible."];
                }
  
    
        // Delete Customer from Database


           
              
                $TaxType->delete();
                return [ "message" =>"Taxtype delete successful."];

       }else{

        return [ "message" =>"Taxtype delete unsuccessful.Record not found."];

       }
        

    
        }else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}







}
