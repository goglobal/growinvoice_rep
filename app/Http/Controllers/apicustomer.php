<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;
use App\Http\Requests\Application\Customer\Store;
use App\Http\Requests\Application\Customer\Update;
use Spatie\Activitylog\Models\Activity;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class apicustomer extends Controller
{
    //

     public function list(Request $request)
    {


    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
     $customer = DB::table('customers')
            ->join('companies', 'companies.id', '=', 'customers.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')
          ->leftjoin('currencies', 'currencies.id', '=', 'customers.currency_id')
       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('customers.id as customerid','customers.display_name','customers.contact_name','customers.email','customers.phone','customers.website','currencies.name as currency name','customers.currency_id')
        
         ->get();
  
         
     return [ "Customer List"=> $customer];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 



  public function getcustomerdetails(Request $request)
    {


    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token) && isset($data->customerid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
     $customer = DB::table('customers')
            ->join('companies', 'companies.id', '=', 'customers.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')
          ->leftjoin('currencies', 'currencies.id', '=', 'customers.currency_id')
       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token],
           [ 'customers.id','=',$data->customerid]
           
          ])
          ->select('customers.id as customerid','customers.display_name','customers.contact_name','customers.email','customers.phone','customers.website','currencies.name as currency name','customers.currency_id')
        
         ->get();
  
         
     return [ "Customer details"=> $customer];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 




  public function add(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->vat_number)&& isset($data->display_name)&& isset($data->contact_name)&& isset($data->email)&& isset($data->phone))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {


     
        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();

 $custcount = DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_subscription_usage.used')        
         ->get();

$ctAddcount=0;
$compid=0;
 foreach ($custAddcount as $addct)
    {
        $ctAddcount=(int)$addct->value;
        $compid=$addct->id;
    }


$ctcount=0;
 foreach ($custcount as $ct)
    {
        $ctcount=(int)$ct->used;
    }

         if(   $ctcount  < $ctAddcount )
         {

 $customer = Customer::create([
          

            'company_id' =>  $compid,
            'display_name' => $data->display_name,
            'contact_name' => $data->contact_name,
            'email' => $data->email,
            'phone' => $data->phone,           
            'vat_number' => $data->vat_number,
             'website' => null,
              'currency_id' => 1,
        ]);

        // Set Customer's billing and shipping addresses
        $customer->address('billing', null );

       
        $customer->address('shipping', null);
       
        //  $customer->address('shipping', $request->input('shipping'));

        // Add custom field values
        $customer->addCustomFields($request->custom_fields);

        // Record product 
      //  $currentCompany->subscription('main')->recordFeatureUsage('customers');

 DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_subscription_usage.used')        
         ->update(['plan_subscription_usage.used' => ($ctcount+ 1)]);

        return [ "Add customer successful."];


         }

            return [ "Cannot add customer. Subscription Plan Limit Exceed."];


    
         }

         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}


  public function edit(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->vat_number)&& isset($data->display_name)&& isset($data->contact_name)&& isset($data->email)&& isset($data->phone) && isset($data->website) && isset($data->currency_id) && isset($data->customerid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {


     
        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();



$ctAddcount=0;
$compid=0;
 foreach ($custAddcount as $addct)
    {
        $ctAddcount=(int)$addct->value;
        $compid=$addct->id;
    }





 $customer = Customer::where([['company_id', '=', $compid] ,

                                    ['id','=',$data->customerid]]
                                    )->firstOrFail();

        // Update Customer in Database
        $customer->update([
            'display_name' => $data->display_name,
            'contact_name' => $data->contact_name,
            'email' => $data->email,
            'phone' => $data->phone,
            'website' => $data->website,
            'currency_id' => $data->currency_id,
            'vat_number' => $data->vat_number,
        ]);

  

        return [ "Update customer successful."];


        

    
        }else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}

public function delete(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->customerid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {


     
        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();

$custcount = DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_subscription_usage.used')        
         ->get();

$ctAddcount=0;
$compid=0;
 foreach ($custAddcount as $addct)
    {
        $ctAddcount=(int)$addct->value;
        $compid=$addct->id;
    }


$ctcount=0;
 foreach ($custcount as $ct)
    {
        $ctcount=(int)$ct->used;
    }

  
        $customer = Customer::where([['company_id', '=', $compid] ,

                                    ['id','=',$data->customerid]
                                    ])-->firstOrFail();

     
       

        // Delete Customer's Estimates from Database
        if ($customer->estimates()->exists()) {
            $customer->estimates()->delete();
        }

        // Delete Customer's Invoices from Database
        if ($customer->invoices()->exists()) {
            $customer->invoices()->delete();
        }

        // Delete Customer's Payments from Database
        if ($customer->payments()->exists()) {
            $customer->payments()->delete();
        }
 
        // Delete Customer's Addresses from Database
        if ($customer->addresses()->exists()) {
            $customer->addresses()->delete();
        }

        // Delete Customer from Database
        $customer->delete();

        DB::table('plan_subscription_usage')
     ->join('plan_features', 'plan_subscription_usage.feature_id', '=', 'plan_features.id')     
     ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
             [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_subscription_usage.used')        
         ->update(['plan_subscription_usage.used' => ($ctcount- 1)]);
  

        return [ "Customer delete successful."];


        

    
        }else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}





}
