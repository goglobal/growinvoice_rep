<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class apilogin extends Controller
{
    //
     public function index(Request $request)
    {
         $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->email) && isset($data->password))
    {
         $user = array(
    'email' => $data->email,
    'password' => $data->password
        );
    if (Auth::attempt($user)){
     $user =(object) DB::table('users')->where('email', $data->email)->first(); 
  
         $token=Hash::make($user->uid.date("Y-m-d"));
 
        DB::table('users')
                ->where('id', $user->id)
                ->update(['token' => $token]);

    $user1 = DB::table('users')->where('email', $data->email)
    ->join('companies', 'companies.owner_id', '=', 'users.id')
    ->select('users.uid', 'users.first_name','users.last_name','users.token','companies.name as company name')->first(); 
  
    $message=" Login Successful";
    return [ "message"=>$message, "userdetails"=> $user1];
     
}else{

        return ["message" => "Wrong email or password" ];

     }


    }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 
}
