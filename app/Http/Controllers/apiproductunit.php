<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductUnit;
use App\Http\Requests\Application\Settings\ProductUnit\Store;
use App\Http\Requests\Application\Settings\ProductUnit\Update;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;


class apiproductunit extends Controller
{
    

     public function list(Request $request)
    {

    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
     $product_units = DB::table('product_units')
            ->join('companies', 'companies.id', '=', 'product_units.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')
        
       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('product_units.id as product_unitid','product_units.name')
        
         ->get();
  
         
     return [ "productunits List"=> $product_units];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 

 public function getdetails(Request $request)
    {


    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid) && isset($data->token) && isset($data->product_unitid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {

         $product_units = DB::table('product_units')
            ->join('companies', 'companies.id', '=', 'product_units.company_id')        
          ->join('users', 'companies.owner_id', '=', 'users.id')      
       
          ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token],
           [ 'product_units.id','=',$data->product_unitid]
          ])
          ->select('product_units.id as product_unitid','product_units.name')
          ->get();

    
  
         
     return [ "product_unit details"=> $product_units];
         }
         else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    }

      return ["message" =>$message];
    } 




  public function add(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->name))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {



         $currentCompanyid=0;


        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('companies.id')        
         ->get();


         foreach ($custAddcount as $addct)
         {
          $currentCompanyid=$addct->id;
         }

          // // Create Tax Type and Store in Database
        ProductUnit::create([
            'name' => $request->name,
            'company_id' => $currentCompanyid
           
        ]);



        return ["message" => "Add product_unit successful."];


         }

         else{

            $message=" Authentication error.";

          }
         }else{

         $message=" Send Format is not correct.";
     

      } 
 return ["message" =>$message];
    }


  public function edit(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->name)&&  isset($data->product_unitid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {
        $currentCompanyid=0;


        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('companies.id')        
         ->get();


         foreach ($custAddcount as $addct)
         {
          $currentCompanyid=$addct->id;
         }


         $product_unit =  ProductUnit::where([
                                    ['company_id','=',$currentCompanyid],
                                    ['id','=',$data->product_unitid]
                                    ])->firstOrFail();

       
        $product_unit->update([
            'name' => $request->name,
            'percent' => $request->percent,
            'description' => $request->description,
        ]);

  

        return ["message" => "Update product_unit successful."];


        

    
        }else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}

public function delete(Request $request)
    {
    $data =(object)$request->json()->all();
    
    $message=" ";
    if( isset($data->uid)  && isset($data->token)&& isset($data->product_unitid))
    {
       $userverify=DB::table('users')
            ->where([
           [ 'users.uid' ,'=',$data->uid],
           [ 'users.token' ,'=',$data->token]
          ])
          ->select('user.*')        
         ->count();
      if($userverify==1)
      {


     
        $custAddcount = DB::table('plan_features')
        ->join('plans', 'plans.id', '=', 'plan_features.plan_id')     
     ->join('plan_subscriptions', 'plan_subscriptions.plan_id', '=', 'plans.id')      
     ->join('companies', 'plan_subscriptions.company_id', '=', 'companies.id')
      ->join('users', 'companies.owner_id', '=', 'users.id')
           
          ->where([
           [ 'users.uid' ,'=',$data->uid],
          [ 'users.token' ,'=',$data->token],
           ['plan_features.slug','=','customers']
          ])
          ->select('plan_features.value','companies.id')        
         ->get();



        $ctAddcount=0;
        $compid=0;
         foreach ($custAddcount as $addct)
            {
                $ctAddcount=(int)$addct->value;
                $compid=$addct->id;
            }
            $Product_Unit =  ProductUnit::where([
                                    ['company_id','=',$compid],
                                    ['id','=',$data->product_unitid]
                                    ])->firstOrFail();


  
        
    
        // Delete Customer from Database
        $Product_Unit->delete();

       

        return [ "message" =>"Product_Unit delete successful."];


        

    
        }else{

             $message=" Authentication error.";

         }
        }else{

        $message=" Send Format is not correct.";
    } 
 return ["message" =>$message];
}






}
