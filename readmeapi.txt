
/*
[11:39 PM, 9/9/2021] Anirban Fin: inaugment@gmail.com
[11:39 PM, 9/9/2021] Anirban Fin: Grow@2022
[11:39 PM, 9/9/2021] Anirban Fin: This is the login

Administrator

BCxlO*eZKY*?Cc;.8psZrZj4J2E!cKD3

ec2-52-66-200-14.ap-south-1.compute.amazonaws.com


passwrod: BCxlO*eZKY*?Cc;.8psZrZj4J2E!cKD3


*/


Login Api:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/login
method : post 
data type: Json 
 {
    "email":"a@1.1",
    "password":"12345678"
 }

 get customer List : 

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/customerlist
method : post 
data type: Json 
 {
    "uid":"613afd65c29a7",
    "token":"$2y$10$xVgzVtdEHCzHmDyFPL5/IemRJaUPrNHm754LDOqz2yAEp8X7uOb92"
 }


get individual customer detail:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/customerdetails
method : post 
data type: Json 
 {
    "uid":"613afd65c29a7",
    "token":"$2y$10$xVgzVtdEHCzHmDyFPL5/IemRJaUPrNHm754LDOqz2yAEp8X7uOb92",
    "customerid" : 2
 }

 add customer:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/addcustomer
method : post 
data type: Json 
 {
    "uid":"613afd65c29a7",
    "token":"$2y$10$xVgzVtdEHCzHmDyFPL5/IemRJaUPrNHm754LDOqz2yAEp8X7uOb92",
    
    "display_name" :"display_name",
    "contact_name" :"contact_name",
    "email" : "email",
    "phone":"phone",           
    "vat_number" :"vat_number"
 }


 edit customer:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/editcustomer
method : post 
data type: Json 
 {
    "uid":"613afd65c29a7",
    "token":"$2y$10$xVgzVtdEHCzHmDyFPL5/IemRJaUPrNHm754LDOqz2yAEp8X7uOb92",
    
    "customerid" : 2,
    "display_name" :"display_name",
    "contact_name" :"contact_name",
    "email" : "email",
    "phone":"phone",           
    "vat_number" :"vat_number",
    "website":"website",
    "currency_id" : 1       
 }

delete customer:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/deletecustomer
method : post 
data type: Json 
 {
    "uid":"613afd65c29a7",
    "token":"$2y$10$xVgzVtdEHCzHmDyFPL5/IemRJaUPrNHm754LDOqz2yAEp8X7uOb92",
    "customerid" : 2
   
 }


taxtype Api: 


 get taxtype List : 

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/taxtypelist
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K"
}

get individual taxtype detail:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/taxtypedetails
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
      "tax_typeid": 4
}
 add taxtype:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/addtaxtype
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
       "name": "Tax 18%",
            "percent": "18.00",
            "description": "18%gst"
}

 edit taxtype:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/edittaxtype
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
     "tax_typeid": 3,
       "name": "Tax 18%",
            "percent": "18.00",
            "description": "18%gst"
}
delete taxtype:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/deletetaxtype
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
     "tax_typeid": 5
       
}



productunit Api: 


 get productunit List : 

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/productunitlist
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K"
}

get individual productunit detail:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/productunitdetails
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
      "product_unitid": 4
}
 add productunit:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/addproductunit
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
       "name": "kgs"
          
}

 edit productunit:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/editproductunit
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
     "product_unitid": 6,
       "name": "kg"
         
}
delete productunit:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/deleteproductunit
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
     "product_unitid": 6
       
}

currency Api: 


 get currency List : 

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/currencylist
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
     "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K"
}

product Api: 


 get product List : 

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/productlist
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
      "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K"
}

get individual product detail:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/productdetails
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
      "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
      "productid": 4
}
 add product:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/addproduct
method : post 
data type: Json 
{
      "uid": "615eda1744cfd",
      "token": "$2y$10$cvdZ2LUwR6u.NsrGxyDcgu3v9IIBfARF9P/q7AraxLI.9ldvrcwdq",
      "name": "sdfag1xx",
      "unit_id": 11,
      "unitname": "Inch",
      "description": "xxxf",
      "price": 10000,
      "currency_id": null,

      "ProductImage": null,
      "HSNCode": "1234",
      "SKUCode":" "1111",
      "StockWarning": 10,
      "Category": "milk",
      "Stock": 100,
      "WholesalePrice": "100.55",
      "WholesaleQty": 5,
      "PurchasePrice": "90.00",
      "SellPrice": 110.00,
      "MRP": 120.00,
      "tax_type": [
            {
                  "tax_type_id": 13,
                  "name": "Example Tax"
            }
      ]
             
            
          
}

 edit product:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/editproduct
method : post 
data type: Json 
            {
            "uid": "615eda1744cfd",
            "token": "$2y$10$cvdZ2LUwR6u.NsrGxyDcgu3v9IIBfARF9P/q7AraxLI.9ldvrcwdq",
            "productid": 4,
            "name": "sdfag1xx",
            "unit_id": 11,
            "unitname": "Inch",
            "description": "xxxf",
            "price": 10000,
            "currency_id": null,

            "ProductImage": null,
            "HSNCode": "1234",
            "SKUCode":" "1111",
            "StockWarning": 10,
            "Category": "milk",
            "Stock": 100,
            "WholesalePrice": "100.55",
            "WholesaleQty": 5,
            "PurchasePrice": "90.00",
            "SellPrice": 110.00,
            "MRP": 120.00,
            "tax_type": [
                        {
                              "tax_type_id": 13,
                              "name": "Example Tax"
                        }
            ]
         
}
delete product:

URL : http://52.66.200.14/Test/growinvoice_rep/public/api/deleteproduct
method : post 
data type: Json 
{
      "uid": "6130890e7a901",
      "token": "$2y$10$ip7MN1O40Li19d2YDSe3kO2gjuyz5XG1SVrrF0b979vIpEWyBdp/K",
      "productid": 6
       
}